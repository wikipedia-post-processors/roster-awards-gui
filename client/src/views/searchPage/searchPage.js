import React, { useState,Fragment} from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import {Redirect  } from 'react-router-dom';
//appbar
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
//Links
import { Link as RouterLink } from 'react-router-dom';
//Search 
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import HelpIcon from '@material-ui/icons/Help';
import Tooltip from '@material-ui/core/Tooltip';
//Dict of different style classes
const styles =theme =>({
  textCenter: {
    textAlign: "center"
  },
   textMuted: {
    color: "#6c757d"
  },
  toolbar: theme.mixins.toolbar,
})
const settings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1
};
const useStyles = makeStyles(styles);
export default function Cards(props) {
  //set state
const [validSearch, setSearch] = useState(false);
const [querystring, setQuery] = useState(null);
  const classes = useStyles();
//functions
 let datelist=[]
 let setValidDates= () => {
   for(let i=1946;i<2022;i++){
     datelist.push(i)
   }
 }
 let handleSubmit= (e) => {
  e.preventDefault();
  let startYear=document.getElementById("startYear").value
  let endYear=document.getElementById("endYear").value
  let league=document.getElementById("league").value
  let name=document.getElementById("name").value
  if (name.length===0){
    return
  }
  let namesplit=name.split(" ")
  if (namesplit.length===2){
    let firstname=namesplit[0]
    let lastname=namesplit[1]
    let querystring=`/playerspage?firstname=${firstname}&lastname=${lastname}&startyear=${startYear}&endyear=${endYear}&league=${league}`
    setQuery(querystring)
    setSearch(true)
  }
  
}
setValidDates()
 document.body.setAttribute("style", "background-color: #C2C2C2");
 if (validSearch) {
    return <Redirect to={querystring} />
   }
 const searchLink = React.forwardRef((props, ref) => (
   <RouterLink ref={ref} to="/" style={{color:"White"}}{...props}  />
 ));
 const apiLink = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/api_info_info" style={{color:"White"}}{...props}  />
  
));
const aboutLink = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/about" style={{color:"White"}}{...props}  />
  
));
  return (
    <Fragment>
    <AppBar style={{ backgroundColor: '#7F947A'}}>
    <Toolbar>
            <IconButton color="inherit" component={searchLink} to="/">
         Search
          </IconButton>
          <IconButton color="inherit" component={aboutLink} to="/about">
        Contact
          </IconButton>
     
          <IconButton color="inherit" component={apiLink} to="/api_info">
         API
          </IconButton>
</Toolbar>
</AppBar>
    <div className={classes.toolbar} />
    <h1 style={{ textAlign: 'center'}}>Pro Basketball Player Awards</h1>
    <h3 style={{ textAlign: 'center'}}>Generate a graphical list of Awards</h3>
 <div style={{display: "grid",gridTemplateRows: "25vh 25vh 25vh 25vh ",gridTemplateColumns: "60vw 10vw 10vw 10vw"}}>
<form className={classes.root} noValidate autoComplete="off" style={{gridRowStart: 2}} onSubmit={handleSubmit}>
      <TextField id="name" label="Search Players Name" variant="outlined" style={{ backgroundColor: 'White',width:"90%",left:"5%"}} />
      <Tooltip title="Search a NBA or WNBA Player by fullname i.e LeBron James. Results Page will Show every award in the period searched.  Warning:Large Searches can take a few minutes to generate" placement="left" leaveDelay="2000">
        <IconButton aria-label="help">
          <HelpIcon/>
        </IconButton>
      </Tooltip>
    
    </form>
    <FormControl variant="filled" className={classes.formControl} style={{gridRowStart: 2}}>
        <InputLabel htmlFor="filled-age-native-simple" style={{color:"white"}}>Start Year</InputLabel>
        <Select
        style={{backgroundColor:"#7F947A",color:"white"}}
          native
          inputProps={{
            name: 'startYear',
            id: 'startYear',
          }}
        >
     {datelist.map((val) => (
          <option value={val} style={{backgroundColor:"#7F947A"}}>{val}</option>   
           ))}
        </Select>
      </FormControl>
      <FormControl variant="filled" className={classes.formControl} style={{gridRowStart: 2}}>
        <InputLabel htmlFor="filled-age-native-simple" style={{color:"white"}}>End Year</InputLabel>
        <Select
        style={{backgroundColor:"#7F947A",color:"white"}}
        defaultValue={2021}
          native
          inputProps={{
            name: 'endYear',
            id: 'endYear',
          }}
        >
     {datelist.map((val) => (
          <option value={val} style={{backgroundColor:"#7F947A"}}>{val}</option>   
           ))}
        </Select>
      </FormControl>

      <FormControl variant="filled" className={classes.formControl} style={{gridRowStart: 2}}>
        <InputLabel htmlFor="filled-age-native-simple" style={{color:"white"}}>League</InputLabel>
        <Select
        style={{backgroundColor:"#7F947A",color:"white"}}
          native
          inputProps={{
            name: 'league',
            id: 'league',
          }}
        >
      <option value={"NBA"} style={{backgroundColor:"#7F947A"}}>{"NBA"}</option>   
      <option value={"WNBA"} style={{backgroundColor:"#7F947A"}}>{"WNBA"}</option>   

        </Select>
      </FormControl>
    
  
   
</div>
      </Fragment>
  );
}
