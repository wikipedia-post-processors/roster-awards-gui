import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
// nodejs library that concatenates classes
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
//Card
import { cardTitle } from "assets/jss/material-kit-react.js";
//appbar
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
//Links
import { Link as RouterLink } from 'react-router-dom';
//Dict of different style classes
const styles = theme => ({
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
  textMuted: {
    color: "#6c757d"
  },
  table: {
    padding: "15px",
    fontSize: "30px"
  },
  tablestart: {
    fontSize: "30px"
  },
  toolbar: theme.mixins.toolbar,
})
const useStyles = makeStyles(styles);
export default function Cards(props) {
  //set state
  const classes = useStyles();
  //functions

  document.body.setAttribute("style", "background-color: #C2C2C2");
  const searchLink = React.forwardRef((props, ref) => (
    <RouterLink ref={ref} to="/" style={{ color: "White" }}{...props} />
  ));
  const apiLink = React.forwardRef((props, ref) => (
    <RouterLink ref={ref} to="/api_info" style={{ color: "White" }}{...props} />
  ));
  const aboutLink = React.forwardRef((props, ref) => (
    <RouterLink ref={ref} to="/about" style={{ color: "White" }}{...props} />

  ));
  let awards_ex = {
    name: 'Stephen Curry',
    teams: [ 'GoldenStateWarriors' ],
    awardsperiod: 22,
    startyear: 2000,
    endyear: 2015,
    GoldenStateWarriors: {
      '2009': [ ' All-Rookie First Team: ' ],
      '2010': [
        ' 4× NBA free-throw percentage leader: ',
        ' NBA Skills Challenge champion: ',
        ' NBA Sportsmanship Award: '
      ],
      '2012': [ ' 6× NBA three-point field goals leader: ' ],
      '2013': [
        ' 2× Second team: ',
        ' 6× NBA three-point field goals leader: ',
        ' NBA Community Assist Award: '
      ],
      '2014': [
        ' 3× NBA champion: ',
        ' 2× NBA Most Valuable Player: ',
        ' 7× NBA All-Star: ',
        ' 3× First team: ',
        ' 6× NBA three-point field goals leader: ',
        ' 4× NBA free-throw percentage leader: ',
        ' 2× NBA Three-Point Contest champion: '
      ],
      '2015': [
        ' 2× NBA Most Valuable Player: ',
        ' 7× NBA All-Star: ',
        ' 3× First team: ',
        ' 2× NBA scoring leader: ',
        ' 6× NBA three-point field goals leader: ',
        ' 4× NBA free-throw percentage leader: ',
        ' NBA steals leader: '
      ],
      active: [ 2009, 2010, 2012, 2013, 2014, 2015 ],
      total: 22,
      team: 'Golden State Warriors'
    }
  }
  awards_ex = JSON.stringify(awards_ex, null, '\t');
  let specificplayerex = {
    Players: [ 'stephencurry' ],
    NBA: 206,
    WNBA: 884,
    stephencurry: {
      '2000': { NBA: 11, WNBA: 1 },
      '2001': { NBA: 13, WNBA: 1 },
      '2002': { NBA: 13, WNBA: 1 },
      '2003': { NBA: 13, WNBA: 1 },
      '2004': { NBA: 13, WNBA: 1 },
      '2005': { NBA: 13, WNBA: 1 },
      '2006': { NBA: 13, WNBA: 1 },
      '2007': { NBA: 13, WNBA: 1 },
      '2008': { NBA: 13, WNBA: 1 },
      '2009': { NBA: 13, WNBA: 1 },
      '2010': { NBA: 13, WNBA: 16 },
      '2011': { NBA: 13, WNBA: 16 },
      '2012': { NBA: 13, WNBA: 16 },
      '2013': { NBA: 13, WNBA: 16 },
      '2014': { NBA: 13, WNBA: 16 },
      '2015': { NBA: 13, WNBA: 51 },
      '2016': { NBA: 0, WNBA: 51 },
      '2017': { NBA: 0, WNBA: 52 },
      '2018': { NBA: 0, WNBA: 155 },
      '2019': { NBA: 0, WNBA: 163 },
      '2020': { NBA: 0, WNBA: 161 },
      '2021': { NBA: 0, WNBA: 161 }
    }
  }
  specificplayerex = JSON.stringify(specificplayerex, null, '\t');
  let allplayerex = {
  Players: [ 'stephencurry', 'anthonybennett', 'anotherbasketballplayer' ],
  NBA: 146,
  WNBA: 992,
  stephencurry: {
    '2000': { NBA: 5, WNBA: 1 },
    '2001': { NBA: 5, WNBA: 1 },
    '2002': { NBA: 5, WNBA: 1 },
    '2003': { NBA: 5, WNBA: 1 },
    '2004': { NBA: 5, WNBA: 1 },
    '2005': { NBA: 5, WNBA: 1 },
    '2006': { NBA: 5, WNBA: 1 },
    '2007': { NBA: 5, WNBA: 1 },
    '2008': { NBA: 5, WNBA: 1 },
    '2009': { NBA: 5, WNBA: 1 },
    '2010': { NBA: 5, WNBA: 16 },
    '2011': { NBA: 5, WNBA: 16 },
    '2012': { NBA: 5, WNBA: 16 },
    '2013': { NBA: 5, WNBA: 16 },
    '2014': { NBA: 5, WNBA: 16 },
    '2015': { NBA: 5, WNBA: 51 },
    '2016': { NBA: 0, WNBA: 51 },
    '2017': { NBA: 0, WNBA: 52 },
    '2018': { NBA: 0, WNBA: 155 },
    '2019': { NBA: 0, WNBA: 163 },
    '2020': { NBA: 0, WNBA: 161 },
    '2021': { NBA: 0, WNBA: 161 }
  },
  anthonybennett: {
    '2010': { NBA: 0, WNBA: 9 },
    '2011': { NBA: 0, WNBA: 9 },
    '2012': { NBA: 0, WNBA: 9 },
    '2013': { NBA: 0, WNBA: 9 },
    '2014': { NBA: 0, WNBA: 9 },
    '2015': { NBA: 0, WNBA: 9 },
    '2016': { NBA: 0, WNBA: 9 },
    '2017': { NBA: 0, WNBA: 9 },
    '2018': { NBA: 0, WNBA: 9 },
    '2019': { NBA: 0, WNBA: 9 },
    '2020': { NBA: 0, WNBA: 9 },
    '2021': { NBA: 0, WNBA: 9 }
  },
  anotherbasketballplayer: {
    '2000': { NBA: 3, WNBA: 0 },
    '2001': { NBA: 3, WNBA: 0 },
    '2002': { NBA: 3, WNBA: 0 },
    '2003': { NBA: 3, WNBA: 0 },
    '2004': { NBA: 3, WNBA: 0 },
    '2005': { NBA: 3, WNBA: 0 },
    '2006': { NBA: 3, WNBA: 0 },
    '2007': { NBA: 3, WNBA: 0 },
    '2008': { NBA: 3, WNBA: 0 },
    '2009': { NBA: 3, WNBA: 0 },
    '2010': { NBA: 3, WNBA: 0 },
    '2011': { NBA: 3, WNBA: 0 },
    '2012': { NBA: 3, WNBA: 0 },
    '2013': { NBA: 3, WNBA: 0 },
    '2014': { NBA: 3, WNBA: 0 },
    '2015': { NBA: 3, WNBA: 0 },
    '2016': { NBA: 3, WNBA: 0 },
    '2017': { NBA: 3, WNBA: 0 },
    '2018': { NBA: 3, WNBA: 0 },
    '2019': { NBA: 3, WNBA: 0 },
    '2020': { NBA: 3, WNBA: 0 },
    '2021': { NBA: 3, WNBA: 0 }
  }
}

  allplayerex = JSON.stringify(allplayerex, null, '\t');
  return (
    <Fragment>
      <Helmet>
        <title>App Title</title>
        <meta name="description" content="App Description" />
        <meta name="theme-color" content="#008f68" />
      </Helmet>
      <AppBar style={{ backgroundColor: '#7F947A' }}>
        <Toolbar>
          <IconButton color="inherit" component={searchLink} to="/">
            Search
          </IconButton>
          <IconButton color="inherit" component={aboutLink} to="/about">
            Contact
          </IconButton>

          <IconButton color="inherit" component={apiLink} to="/api_info">
            API
          </IconButton>
        </Toolbar>
      </AppBar>
      <div className={classes.table} />
      <h1> <b> Endpoint</b></h1>
      <span className={classes.table}>/awards_api</span>

      <h2><b> Params</b></h2>
      <table class="data">
        <tr>
          <th className={classes.tablestart}>Field</th>
          <th className={classes.table}>Type</th>
          <th className={classes.table}>Required</th>
        </tr>
        <tr>
          <td className={classes.tablestart}>firstname</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>Yes</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>lastname</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>Yes</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>startyear</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>Yes</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>endyear</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>Yes</td>
        </tr>
      </table>
      <h2> <b> Example Output</b></h2>
      <pre>
        {awards_ex}
      </pre>
      <h1> <b> Endpoint</b></h1>
      <span className={classes.table}>/usage_stats</span>

      <h2><b> Params</b></h2>
      <table class="data">
        <tr>
          <th className={classes.tablestart}>Field</th>
          <th className={classes.table}>Type</th>
          <th className={classes.table}>Required</th>
        </tr>
        <tr>
          <td className={classes.tablestart}>firstname</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>No</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>lastname</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>No</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>startyear</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>No</td>
        </tr>
        <tr>
          <td className={classes.tablestart}>endyear</td>
          <td className={classes.table}>String</td>
          <td className={classes.table}>No</td>
        </tr>
  Note:Most include first name and last name, or neither
</table>
      <h2> <b> Example Output</b></h2>
      <h4> <b> Specific Players</b></h4>
      <pre>
        {specificplayerex}
      </pre>
      <h4> <b> All Players</b></h4>
      <h2> <b> Real output goes from 1946-current. Example is Shortened</b></h2>
      <pre>
        {allplayerex}
      </pre>
    </Fragment>
  );
}
