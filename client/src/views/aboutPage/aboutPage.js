import React, { Fragment} from 'react';
import { Helmet } from 'react-helmet';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
//Card
import { cardTitle } from "assets/jss/material-kit-react.js";
//appbar
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
//Links
import { Link as RouterLink } from 'react-router-dom';
import Link from '@material-ui/core/Link';
//Dict of different style classes
const styles =theme =>({
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
   textMuted: {
    color: "#6c757d"
  },
  table:{
    padding:"15px",
    fontSize:"30px"
  },
  tablestart:{
    fontSize:"30px"
  },
  toolbar: theme.mixins.toolbar,
})
const useStyles = makeStyles(styles);
export default function Cards(props) {
  const classes = useStyles();
//functions
  
 document.body.setAttribute("style", "background-color: #C2C2C2");
 const searchLink = React.forwardRef((props, ref) => (
   <RouterLink ref={ref} to="/" style={{color:"White"}}{...props}  />
 ));
 const apiLink = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/api_info" style={{color:"White"}}{...props}  />
));
const aboutLink = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/about" style={{color:"White"}}{...props}  />
  
));
  return (
    <Fragment>
        <Helmet>
  <title>App Title</title>
  <meta name="description" content="App Description" />
  <meta name="theme-color" content="#008f68" />
</Helmet>
    <AppBar style={{ backgroundColor: '#7F947A'}}>
    <Toolbar>
            <IconButton color="inherit" component={searchLink} to="/">
         Search
          </IconButton>
          <IconButton color="inherit" component={aboutLink} to="/about">
         Contact
          </IconButton>
     
          <IconButton color="inherit" component={apiLink} to="/api_info">
         API
          </IconButton>
</Toolbar>
</AppBar>
<div className={classes.toolbar} style={{display: "grid",gridTemplateRows: "10vh 10vh 20vh 20vh 20vh ",gridTemplateColumns: "25vh 25vh 25vh 25vh "}}>
<div style={{gridRowStart: 2,gridColumnStart :1,gridColumnEnd :5,paddingLeft: '10px'}}>
<p style={{fontSize:"70px",lineHeight:1.5}}>Author: Tobi Fanibi</p>
<br></br>
<p style={{fontSize:"70px",lineHeight:1.5}} >
Contact: 
 <Link href="https://gitlab.com/wikipedia-post-processors/roster-awards-gui/-/issues" style={{paddingLeft: '10px'}}>
    GitLab
  </Link>
  </p>
  </div>
   </div>
      </Fragment>
  );
}
