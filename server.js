// Import required modules
const express = require('express');
const bodyParser = require('body-parser');
var flatCache = require('flat-cache')
var cache = flatCache.load('cacheId');
const app = express();
const port = process.env.PORT || 5000;
const fs = require("fs");
app.use(bodyParser.json());
require('dotenv').config()
//args
const args = process.argv.slice(2)
let nba = args[0]
let wnba = args[1]
let dbname = args[2]
let dburl = args[3]
let dbuser = args[4]
let dbpass = args[5]
//fetch
const fetch = require('node-fetch');
const timeout = setTimeout(
  () => { controller.abort(); },
  150,
);
const AbortController = require("abort-controller")
const controller = new AbortController();


var mysql = require('mysql2');
const year = new Date().getFullYear()
//endpoints
app.get('/awards_api', function (req, res) {
  let fname = req.query.firstname
  let lname = req.query.lastname
  let league = req.query.league
  let start = parseInt(req.query.startyear || 1946)
  let end = parseInt(req.query.endyear || year) 
  let url=nba
  if (!fname || !lname ||!league ||!start ||!end) {
    res.send({ "error": "Missing a paramater required: firstname Lastname Space seperated\nleaguage NBA or WNBA\nstartyear and endyear" })
    return
  }
  if (league=="WNBA"){
    url=wnba
  }
  data = getplayer_awards(start, end, fname, lname, res,url)
  //Api is limited we will save data as name+year
  //loop through year data+ person name
  for (let curryear = start; curryear < end + 1; curryear++) {
    savedata(fname, lname, league,curryear)
  }
});
app.get('/playerpics', function (req, res) {
  if(!req.query.firstname || !req.query.lastname){
    res.send({"error":"Missing firstname or lastname param"})
    return
  }
  let fname = req.query.firstname.toLowerCase()
  let lname = req.query.lastname.toLowerCase()
  getBasketPics(`${fname} ${lname} basketball`,150,res)
});
app.get('/teampic', function (req, res) {
  let team = req.query.team.toLowerCase()
  getBasketPics(`${team} logo current`,1, res)
});
app.get('/usage_stats', function (req, res) {
  let tablename = null
  let start = req.query.startyear || 1946
  let end = req.query.endyear || year
  if (!req.query.firstname || !req.query.lastname) {
    output(tablename, getNames(), start, end, res)
  }
  else {
    output(tablename, [`${req.query.firstname} ${req.query.lastname}`], start, end, res)
  }
})
//Helper Functions
function getNames() {
  let names = cache.getKey('names')
  if (names == undefined)
    return []
  else {
    return names
  }
}
function addNames(namelist, fname, lname) {
  let name = `${fname} ${lname}`.toLowerCase()
  if (namelist.includes(name))
    null
  else {
    namelist.push(name)
  }
  return namelist
}
function get_tablename(name) {
  return new Promise((resolve, reject) => {
    let namesplit = name.split(" ")
    let fname = namesplit[0]
    let lname = namesplit[1]
    let playerdict = {}
    let tablename = `${fname}${lname}`.toLowerCase()
    resolve(tablename)
  })
}
function deleteCache(){
  let keys=Object.keys(cache.all())
  for(let index=0;index<keys.length;index++){
    let key=keys[index]
    cache.removeKey(key)
    cache.save(true /* noPrune */)
  }
}
//dbfunctions
function createtable(connection, tablename) {
  let createtable = `create table if not exists ${tablename}(
    id int primary key auto_increment,
    firstname varchar(255)not null,
    lastname varchar(255)not null,  
    year int not null,    
    NBA int default 0,  
    WNBA int default 0                       
)`;
  return new Promise((resolve, reject) => {
    connection.query(createtable, tablename, function (err, results, data) {
      if (err) {
        console.log(err)
        reject()
      }
      resolve()
    });
  })
}
function findrow(
  connection, tablename, year) {
  let findrow = `SELECT * FROM ${tablename} WHERE year=?`;
  return new Promise((resolve, reject) => {
    connection.query(findrow,year, function (err, results, data) {
      if (err) {
        reject()
      }
      resolve(results[0])
    })
  })
}
function insertrow(connection, tablename, newdata,league) {
  let nba_count=0
  let wnba_count=0
  if (league=="WNBA"){
    wnba_count=1
  }
  newdata.push(nba_count)
  newdata.push(wnba_count)
  let insertrow = `INSERT INTO ${tablename} (firstname,lastname,year,NBA,WNBA) VALUES (?,?,?,?,?)`
  return new Promise((resolve, reject) => {
    connection.query(insertrow,newdata, function (err, results, data) {
      if (err) {
        reject()
      }
      resolve(data)
    })
  })
}
function updaterow(connection, tablename, year,count,league) {
  let updaterow = `UPDATE ${tablename} SET ${league}=? WHERE year=?`
  return new Promise((resolve, reject) => {
    connection.query(updaterow, [count,year],function (err, results, data) {
      if (err) {
        console.log(err)
        reject()
      }
      resolve(data)
    })
  })
}
function get_table(connection, tablename,curryear = null) {
  return new Promise((resolve, reject) => {
    let get_table = null
    if (curryear!=null) {
      get_table = `SELECT * FROM ${tablename} WHERE year =?`;
      connection.query(get_table, curryear, function (err, results, data) {
        if (err) {
          console.log(err);
          reject()
        }
        else if(results.length==0) {
          resolve({})
        }
        else{
          resolve(results[0])
        }
      })
    }
    else {
      get_table = `SELECT * FROM ${tablename}`
      connection.query(get_table, function (err, results, data) {
        if (err) {
          console.log(err);
          reject(null)
        }
        if (!results) {
          resolve("No data for Player")
        }
        else {
          resolve(results[0])
        }
      })
    }
  })
}
function release(connection) {
  connection.release(function (err) {
    if (err) {
      reject(err)
    }
  });
}
function output(tablename, namelist, start, end, res) {
  if (namelist.length == 0) {
    res.send("No Data in DB")
    return
  }
  pool.getConnection(async function (err, connection) {
    outdict = {}
    outdict["Players"] = []
    outdict["NBA"] = 0
    outdict["WNBA"] = 0
    for (let index = 0; index < namelist.length + 1; index++) {
      if (index == namelist.length) {
        console.log(outdict)
        release(connection)
        res.send(outdict)
        break
      }
      let name = namelist[index]
      let playerdict = {}
      console.log("Currently Checking", name, namelist)
      let tablename = await get_tablename(name)
      outdict["Players"].push(tablename)
      for (let curryear=parseInt(start); curryear < parseInt(end) + 1; curryear++) {
        console.log(curryear)
        data = await get_table(connection, tablename).catch(e => {
          console.log(e)
        })
        if (data == "No data for Player" || data == null) {
          playerdict["error"] = "Player Has Never Been Searched"
          break
        }
        playerdict[curryear]={}
        data = await get_table(connection, tablename, curryear).catch(e => {
          console.log(e)
        })
        if (Object.keys(data).length==0){
        playerdict[curryear]["NBA"] = 0
        playerdict[curryear]["WNBA"] = 0
        }
        else{     
        outdict["NBA"] = outdict["NBA"] + data["NBA"]
        outdict["WNBA"] = outdict["WNBA"] + data["WNBA"]
        playerdict[curryear]["NBA"] = data["NBA"]
        playerdict[curryear]["WNBA"] = data["WNBA"]
        }

      }
      outdict[tablename] = playerdict
    }
    release(connection)
  })
}
function savedata(firstname, lastname,league, year) {
  namelist = getNames()
  namelist = addNames(namelist, firstname, lastname)
  cache.setKey('names', namelist);
  cache.save(true /* noPrune */)
  pool.getConnection(async function (err, connection) {
    if (err) {
      console.log(err);

      return 
    }
    tablename = `${firstname}${lastname}`.toLowerCase()
    //Create new table if needed
    await createtable(connection, tablename).catch(e => {
      console.log(e)
    })
    //check if data exist
    let data = await findrow(connection, tablename, year).catch(e => {
  console.log(e)
    }) || {}
    //insert row
    if (Object.keys(data).length == 0) {
      let newdata = [firstname, lastname, year]
      insertrow(connection, tablename, newdata,league).catch(e => {
       console.log(e)
      })
    }
    //update row 
    else {
      updaterow(connection, tablename, year,data[league]+1,league).catch(e => {
       console.log(e)
      })
    }
    release(connection)
  })
}
async function getplayer_awards(start, end, fname, lname, res,hostname) {
  fname = fname.toLowerCase()
  lname = lname.toLowerCase()
  fname = fname[0].toUpperCase() + fname.slice(1)
  lname = lname[0].toUpperCase() + lname.slice(1)
  award_dict = {}
  award_dict["name"] = `${fname} ${lname}`
  award_dict["teams"] = []
  award_dict["awardsperiod"] = 0
  award_dict["startyear"] = start
  award_dict["endyear"] = end
  for (let curryear = start; curryear < end + 1; curryear++) {
    url = `https://${hostname}/api/player/year/${curryear}?firstname=${fname}&lastname=${lname}`
    let json = null
    console.log(url)
    try {
      response = await fetch(url);
      json = await response.json();
    }
    catch (e) {
      continue
    }
    finally {
      null
    }
    if (json == "NO_PLAYER_FOUND" || json.awards == "NONE_FOUND") {
      continue
    }
    for (let index = 0; index < json.stats.length; index++) {
      let team = json.stats[index].team
      team=team.trim()
      currteam = team.split(" ").join("")
      let awards = json.awards
      if (!award_dict["teams"].includes(currteam)) {
        award_dict["teams"].push(currteam)
        teamdict = {}
        teamdict["active"] = [curryear]
        teamdict["total"] = awards.length
        teamdict[curryear] = awards
        teamdict["team"] = team
        award_dict[currteam] = teamdict
        award_dict["awardsperiod"] = award_dict["awardsperiod"] + awards.length;
      }
      else {
        teamdict = award_dict[currteam]
        teamdict["active"].push(curryear)
        teamdict["total"] = teamdict["total"] + awards.length
        teamdict["team"] = team
        teamdict[curryear] = awards
        award_dict[currteam] = teamdict
        award_dict["awardsperiod"] = award_dict["awardsperiod"] + awards.length;
      }
    }
  }
  res.send(award_dict)
}
var pool = mysql.createPool({
  connectionLimit: 5,
  host: dburl,
  user: dbuser,
  password: dbpass,
  database: dbname
});
async function getBasketPics(name, num,res) {
  deleteCache()
  let picout = cache.getKey(`${name}pic`) || []
  console.log(`${name}pic- Number of Pics in Cache:${picout.length}`)
  if (picout.length == 0) {
    let picwidth = 0
    let picheight = 0
    let quality = 0
    urlbase = `https://bing-image-search1.p.rapidapi.com/images/search?q=${name}&mkt=%20en-US&safeSearch=strict&count=${num}`
    const options = {
      "method": "GET",
      "port": null,
      "headers": {
        "x-rapidapi-key": process.env.BING_KEY,
        "x-rapidapi-host": "bing-image-search1.p.rapidapi.com",
        "useQueryString": true,
      }
    };
    url = new URL(urlbase);
    const response = await fetch(url, options);
    let json = await response.json();
    let piclist = json.value
    if (piclist.length == 0) {
      picurl = "https://images.unsplash.com/photo-1515523110800-9415d13b84a8?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1834&q=80"
      let picwidth = 1834
      let picheight = 2751
      picdata = { "url": picurl, "width": picwidth, "height": picheight }
      picout.push(picdata)
    }
    for (i = 0; i < piclist.length; i++) {
      console.log("checking pic number:",i)
      tempic = piclist[i]        
      picurl = tempic.contentUrl

      picheight = tempic.height
      picwidth = tempic.width
      picdata = { "url": picurl, "width": picwidth, "height": picheight }
      let tempquality = tempic.height * tempic.width
      if (tempquality > quality) {
        quality = tempquality
        picout.unshift(picdata)
      }
      else{
        picout.push(picdata)

      }
    }
  }
  cache.setKey(`${name}pic`, picout);
  cache.save(true /* noPrune */)
  res.send(picout)
}
app.listen(port, () => console.log(`Listening on port ${port}`));
