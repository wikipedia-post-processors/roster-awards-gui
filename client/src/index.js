import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import "assets/scss/material-kit-react.scss?v=1.9.0";
// pages for this product
import playersPage from "views/playersPage/playersPage.js";
import apiPage from "views/apiPage/apiPage.js";
import searchPage from "views/searchPage/searchPage.js";
import aboutPage from "views/aboutPage/aboutPage.js";
var hist = createBrowserHistory();
ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Route path="/playerspage" component={playersPage} />
      <Route path="/api_info" component={apiPage} />
      <Route path="/about" component={aboutPage} />
      <Route path="/" component={searchPage} />
    </Switch>
  </Router>,
  document.getElementById("root")
);
