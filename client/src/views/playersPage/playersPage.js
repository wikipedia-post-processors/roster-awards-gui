import React, { useState ,useEffect,Fragment} from 'react';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import {useLocation } from 'react-router-dom';
//Card
import Card from "components/Card/Card.js";
import { cardTitle } from "assets/jss/material-kit-react.js";
//appbar
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
//Links
import { Link as RouterLink } from 'react-router-dom';
//info 
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
//image Carousel for player
import Carousel from "react-slick";
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
//Dicts of settings
const styles =theme =>({
  cardTitle,
  textCenter: {
    textAlign: "center"
  },
   textMuted: {
    color: "#6c757d"
  },
  toolbar: theme.mixins.toolbar,
})

const settings = {
  infinite: true,
  slidesToShow: 1,
  lazyLoad:"ondemand"
};
const useStyles = makeStyles(styles);
export default function Cards(props) {
  //set state
const params=useLocation().search
 let paramsdata=new URLSearchParams(params)
  const classes = useStyles();
  const [items, setItems] = useState([]);
  const [playerimgs, setplayer_imgs] = useState([]);
  const [vertical, setVert] = useState('top');
  const [horizontal, setHor] = useState('center');
  const [open, setOpen] = React.useState(true);
//functions
let setplayerPic = async () => {
  const playeres = await fetch('/playerpics'.concat(params));   
  if (playeres.status !== 200) throw Error(playeres.message);
  const playerpics = await playeres.json()
  setplayer_imgs(playerpics)
}

  let create_Card = async () => {
  if (!paramsdata.get("league") || !paramsdata.get("startyear") || !paramsdata.get("endyear") ||  !paramsdata.get("firstname") ||  !paramsdata.get("lastname")) {
    return ["Missing Argument"]
  }
    console.log("Trying to Fetch Data")
    const awards_response = await fetch('/awards_api'.concat(params));
    if (awards_response.status !== 200) throw Error(awards_response.message);
    const awards_dict = await awards_response.json()
    const playerpics_response = await fetch('/playerpics'.concat(params));  
    if (playerpics_response.status !== 200) throw Error(playerpics.message);
    const playerpics= await playerpics_response.json()

    setOpen(false);
    let teamslist=awards_dict["teams"]
    let playername=awards_dict["name"]
    let enddex=0
    setItems(items=>[...items,<div><h1 style={{ color: 'black',paddingLeft:"30%"}}>{playername}</h1></div>])
    if (awards_dict.awardsperiod==0){
      setItems(items=>[...items,<h1 style={{ paddingLeft: '30%'}}>No Awards Found</h1>])
    }
    //by team
    for (let team of teamslist)  
      {
      //team name correct format
      let t=awards_dict[team]["team"]
      //Logo of team from wiki
      let teamname=<AppBar position="static" style={{ backgroundColor: '#947A7F'}}>
      <Toolbar>
        <Button component={RouterLink} disabled><h3 style={{ color: 'White'}}>{t}</h3></Button>
      </Toolbar>
    </AppBar>
    setItems(items=>[...items,teamname])
    const teamres = await fetch(`/teampic?team=${t}`);  
    if (teamres.status !== 200) throw Error(teamres.message);
    let teampic = await teamres.json()
    teampic=teampic[0]
    setItems(items=>[...items,<img src={teampic.url} style={{ maxWidth: '50%',maxHeight :'50%', margin: 'auto',display:'block'}}></img>])
      let total=awards_dict[team]["total"]
      let team_ele= 
     <h3 style={{ paddingLeft: '15px'}}>{total} Awards Won During Period</h3>
     setItems(items=>[...items,team_ele])
      //by year
      for (let year of awards_dict[team]["active"]){
        let year_ele=<Button variant="contained" style={{ backgroundColor: '#7F947A',color:'white'}}>{year}</Button>
        let awards=awards_dict[team][year]
        console.log(awards)
        if (awards.length>0)
        {
          setItems(items=>[...items,year_ele])
          //by award
          for (let award of awards){
          let award_ele=
          <div>
          <h6 style={{ paddingLeft: '20px'}}>{award}</h6>
          <br></br></div>
           setItems(items=>[...items,award_ele])
      
        
          }
        }
        //check if awards if not skip
       
     
      }
    
      }
    return 
    
    
}
useEffect(() => {
  create_Card()
  setplayerPic()
}, [])
 document.body.setAttribute("style", "background-color: #C2C2C2");
 const searchLink = React.forwardRef((props, ref) => (
  <RouterLink ref={ref} to="/" style={{color:"White"}}{...props}  />
));
const apiLink = React.forwardRef((props, ref) => (
 <RouterLink ref={ref} to="/api_info" style={{color:"White"}}{...props}  />
 
));
const aboutLink = React.forwardRef((props, ref) => (
 <RouterLink ref={ref} to="/about" style={{color:"White"}}{...props}  />
 
));
const handleClose = () => {
  setOpen(false);
};
 return (
   <Fragment>
   <AppBar style={{ backgroundColor: '#7F947A'}}>
   <Toolbar>
           <IconButton color="inherit" component={searchLink} to="/">
        Search
         </IconButton>
         <IconButton color="inherit" component={aboutLink} to="/about">
        Contact
         </IconButton>
    
         <IconButton color="inherit" component={apiLink} to="/api_info">
        API
         </IconButton>
</Toolbar>
</AppBar>
    <div className={classes.toolbar} />
<div style={{display: "grid",gridTemplateRows: "40vh",gridTemplateColumns: "20vw 60vw 10vw 10vw"}}>
<Snackbar open={open} autoHideDuration={6000000} onClose={handleClose} anchorOrigin={{ vertical, horizontal }}>
  <Alert onClose={handleClose} severity="info" style={{backgroundColor:"#947A7F"}}>
  Please Wait for Results to Load
              <br></br>
              Wide Year Ranges can take a Moment
  </Alert>
</Snackbar>

<div style={{gridRowStart:1,gridRowEnd:1,gridColumnStart:2,display:"block"}}>
<Carousel>
{playerimgs.map((val,index) => (
          <div key={index}>
          <img
                
                style={{maxHeight: "calc(40vh)",maxWidth: "calc(40vh)",marginLeft:"35%"}}
                src={val.url}
                className="slick-image"
              />
            </div>

       ))}
          </Carousel>
          </div>





<Card style={{ backgroundColor: '#C2C2C2',gridColumnStart:2,display:"block"}}>

{items.map((val,index) => (
          <div key={index}> 
          {val}
          </div>
       ))}
  
  
  
  
 

</Card>
</div>
      </Fragment>
  );
}
